/* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
const ENDPOINT_BASE_URL = "https://sporadic.nz/pokesignment/";


/* TODO: Your code here */
function main(){
    let burger_icon = document.getElementById("nav_menu_icon");
    let drop_nav_menu = document.getElementById("mobile_nav_menu");
    function close_nav_menu() {
        if (document.body.clientWidth > 900) {
            drop_nav_menu.style.display = "none";
        }
    }
    let close_mobile_nav = setInterval(close_nav_menu,100);
    function toggle_nav(){
        if (drop_nav_menu.style.display === "none"){
            drop_nav_menu.style.display = "block";
            close_mobile_nav = setInterval(close_nav_menu,100);
        } else {
            drop_nav_menu.style.display = "none";
            clearInterval(close_mobile_nav);
        }
    }
    burger_icon.addEventListener("click",toggle_nav);

    let homebutton = document.getElementById("nav_home");
    homebutton.addEventListener("click",function(){
        window.open("https://www.pokemon.com","_blank")});
}