/* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
const ENDPOINT_BASE_URL = "https://sporadic.nz/pokesignment/";


/* TODO: Your code here */
let img_width;
function imageSauce(sauce){
    return ENDPOINT_BASE_URL+"img/" + sauce;
}
function iChooseYou(pokemon){
    return ENDPOINT_BASE_URL + "pokemon?pokemon=" + pokemon;
}
function getRandomPoke(){
    let pokeBall = new  XMLHttpRequest();
    pokeBall.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4){
            loadPokeToday(JSON.parse(this.responseText));
        }
    };
    pokeBall.open("GET",ENDPOINT_BASE_URL+"pokemon?random=random",true);
    pokeBall.send();
}
function loadPokeToday(data){
    let today_col = document.getElementById("today_pokemon");
    let col_width = today_col.getBoundingClientRect().width; img_width = col_width+"";
    let heading = document.createElement("h4");
    heading.innerText = "Pokemon of the day";
    let img_div = document.createElement("div");
    let img = document.createElement("img");
    img.src = imageSauce(data.image);
    img_div.appendChild(img);
    img.setAttribute("width",col_width+"");
    let pokeName = document.createElement("h5");
    pokeName.innerText = data.name;
    let pokedex = document.createElement("p");
    pokedex.innerText = data.description;
    today_col.innerText = "";
    today_col.appendChild(heading);
    today_col.appendChild(img_div);
    today_col.appendChild(pokeName);
    today_col.appendChild(pokedex);
}
function pic2details(detail_col,pokemon){
    let getPoke = new XMLHttpRequest();
    getPoke.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4){
            let data = JSON.parse(this.responseText);
            let newPic = document.createElement("img");
            let picDiv = document.createElement("span");
            newPic.setAttribute("id",pokemon);
            picDiv.addEventListener("click",function () {
                poke(pokemon);
            });
            newPic.setAttribute("src",imageSauce(data.image));
            newPic.setAttribute("width",img_width);
            picDiv.style.backgroundColor = "#46ACC2";
            picDiv.style.paddingBottom = "1%";
            picDiv.appendChild(newPic);
            let name = document.createElement("h5");
            name.innerText = data.name;
            picDiv.appendChild(name);
            detail_col.appendChild(picDiv);
        }
    };
    getPoke.open("GET",iChooseYou(pokemon),true);
    getPoke.send();
}
function pokeDetails(){
    let pokeList = new XMLHttpRequest();
    let details = document.getElementById("details");
    details.innerText = "";
    let title = document.createElement("h4");
    title.innerText = "Pokemon details";
    details.appendChild(title);
    pokeList.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4){
            let plist = JSON.parse(this.responseText);
            for (let i = 0; i < plist.length; i++) {
                pic2details(details,plist[i]);
            }
        }
    };
    pokeList.open("GET",ENDPOINT_BASE_URL+"pokemon",true);
    pokeList.send();
}
function fillPoke(data,div){
    let name = document.createElement("h4");
    name.innerText = data.name;
    let pic_div = document.createElement("div");
    let pic = document.createElement("img");
    pic.setAttribute("src",imageSauce(data.image));
    pic_div.appendChild(pic);
    let desc = document.createElement("p");
    desc.innerText = data.description;
    div.appendChild(name);
    div.appendChild(pic_div);
    div.appendChild(desc);
}
function fillSide(data,div,type,heading){
    let head = document.createElement("h4");
    head.innerText = heading;
    div.appendChild(head);
    let things = data.opponents[type];
    for (let i = 0; i < things.length; i++) {
        let get = new XMLHttpRequest();
        get.onreadystatechange = function(){
            if (this.status == 200 && this.readyState == 4){
                let pokedata = JSON.parse(this.responseText);
                let pic_div = document.createElement("div");
                let pic = document.createElement("img");
                pic_div.style.backgroundColor = "#46ACC2";
                pic.setAttribute("src",imageSauce(pokedata.image));
                pic.setAttribute("width",img_width);
                pic_div.style.margin = "2%";
                pic_div.addEventListener("click",function () {
                    poke(pokedata.name);
                });
                pic_div.appendChild(pic);
                let name = document.createElement("h5");
                name.innerText = pokedata.name;
                pic_div.style.paddingBottom = "1%";
                pic_div.appendChild(name);
                div.appendChild(pic_div)
            }
        }
        get.open("GET",iChooseYou(things[i]),true);
        get.send();
    }
}
function fillWeak(data,div){
    fillSide(data,div,"weak_against","Weak Against");
}
function fillStrong(data,div){
    fillSide(data,div,"strong_against","Strong Against");
}
function featurePoke(data,details_div){
    let weak = document.createElement("div");
    let strong = document.createElement("div");
    let pokemon = document.createElement("div");
    weak.classList.add("weakness");
    strong.classList.add("strength");
    pokemon.classList.add("featured");
    fillPoke(data,pokemon);
    fillWeak(data,weak);
    fillStrong(data,strong);
    details_div.appendChild(weak);
    details_div.appendChild(pokemon);
    details_div.appendChild(strong);
}
function poke(id){
    let div = document.getElementById("details");
    div.innerHTML = "";
    let pokedex = document.createElement("div");
    pokedex.classList.add("pokedex");
    let get = new XMLHttpRequest();
    get.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4){
            let data = JSON.parse(this.responseText);
            let title = document.createElement("h4");
            title.innerText = "Pokemon details";
            div.appendChild(title);
            featurePoke(data,pokedex);
            div.appendChild(pokedex)
        }
    }
    get.open("GET",iChooseYou(id),true);
    get.send();
}
function main(){
    let burger_icon = document.getElementById("nav_menu_icon");
    let drop_nav_menu = document.getElementById("mobile_nav_menu");
    function close_nav_menu() {
        if (document.body.clientWidth > 900) {
            drop_nav_menu.style.display = "none";
        }
    }
    let close_mobile_nav = setInterval(close_nav_menu,100);
    function toggle_nav(){
        if (drop_nav_menu.style.display === "none"){
            drop_nav_menu.style.display = "block";
            close_mobile_nav = setInterval(close_nav_menu,100);
        } else {
            drop_nav_menu.style.display = "none";
            clearInterval(close_mobile_nav);
        }
    }
    burger_icon.addEventListener("click",toggle_nav);
    let homebutton = document.getElementById("nav_home");
    homebutton.addEventListener("click",function(){
        window.open("https://www.pokemon.com","_blank")});
    getRandomPoke();
    pokeDetails();
}