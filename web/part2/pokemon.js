/* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
const ENDPOINT_BASE_URL = "https://sporadic.nz/pokesignment/";


/* TODO: Your code here */
function imageSauce(sauce){
    return "https://sporadic.nz/pokesignment/img/" + sauce;
}
function getRandomPoke(){
    let pokeBall = new  XMLHttpRequest();
    pokeBall.onreadystatechange = function(){
        if (this.status == 200 && this.readyState == 4){
            loadPokeToday(JSON.parse(this.responseText));
        }
    };
    pokeBall.open("GET","https://sporadic.nz/pokesignment/pokemon?random=random",true);
    pokeBall.send();
}
function loadPokeToday(data){
    let today_col = document.getElementById("today_pokemon");
    let col_width = today_col.getBoundingClientRect().width;
    let heading = document.createElement("h4");
    heading.innerText = "Pokemon of the day";
    let img_div = document.createElement("div");
    let img = document.createElement("img");
    img.src = imageSauce(data.image);
    img_div.appendChild(img);
    img.setAttribute("width",col_width+"");
    let pokeName = document.createElement("h5");
    pokeName.innerText = data.name;
    let pokedex = document.createElement("p");
    pokedex.innerText = data.description;
    today_col.innerText = "";
    today_col.appendChild(heading);
    today_col.appendChild(img_div);
    today_col.appendChild(pokeName);
    today_col.appendChild(pokedex);
}
function main(){
    let burger_icon = document.getElementById("nav_menu_icon");
    let drop_nav_menu = document.getElementById("mobile_nav_menu");
    function close_nav_menu() {
        if (document.body.clientWidth > 900) {
            drop_nav_menu.style.display = "none";
        }
    }
    let close_mobile_nav = setInterval(close_nav_menu,100);
    function toggle_nav(){
        if (drop_nav_menu.style.display === "none"){
            drop_nav_menu.style.display = "block";
            close_mobile_nav = setInterval(close_nav_menu,100);
        } else {
            drop_nav_menu.style.display = "none";
            clearInterval(close_mobile_nav);
        }
    }
    burger_icon.addEventListener("click",toggle_nav);
    let homebutton = document.getElementById("nav_home");
    homebutton.addEventListener("click",function(){
        window.open("https://www.pokemon.com","_blank")});
    getRandomPoke();
}