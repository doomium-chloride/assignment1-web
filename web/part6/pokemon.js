/* Base address for the Pokemon endpoints. Add the endpoint name and parameters onto this */
const ENDPOINT_BASE_URL = "https://sporadic.nz/pokesignment/";


/* TODO: Your code here */
let img_width;
function wipe(element){
    element.innerHTML = "";
}
function make(thing){
    return document.createElement(thing);
}
function full_make(type,classes,appendee){
    let out = make(type);
    for (let i = 0; i < classes.length; i++) {
        out.classList.add(classes[i]);
    }
    appendee.appendChild(out);
    return out;
}
function fmake(type,appendee){
    return full_make(type,[],appendee)
}
function done(obj){
    return (obj.status == 200 && obj.readyState == 4)
}
function imageSauce(sauce){
    return ENDPOINT_BASE_URL+"img/" + sauce;
}
function iChooseYou(pokemon){
    return ENDPOINT_BASE_URL + "pokemon?pokemon=" + pokemon;
}
function getRandomPoke(func){
    let pokeBall = new  XMLHttpRequest();
    pokeBall.onreadystatechange = function(){
        if (done(this)){
            func(JSON.parse(this.responseText));
        }
    };
    pokeBall.open("GET",ENDPOINT_BASE_URL+"pokemon?random=random",true);
    pokeBall.send();
}
function featureRandomPoke(){
    getRandomPoke(function (arg) {
        poke(arg.name);
    })
}
function loadPokeToday(data){
    let today_col = document.getElementById("today_pokemon");
    wipe(today_col);
    let col_width = today_col.getBoundingClientRect().width; img_width = col_width+"";
    let heading = fmake("h4",today_col);
    heading.innerText = "Pokemon of the day";
    let img_div = fmake("div",today_col);
    let img = fmake("img",img_div);
    img.src = imageSauce(data.image);
    img.setAttribute("width",col_width+"");
    let pokeName = fmake("h5",today_col);
    pokeName.innerText = data.name;
    let pokedex = fmake("p",today_col);
    pokedex.innerText = data.description;
    let detail_button = full_make("div",["button"],today_col);
    detail_button.innerText = "Show Details";
    detail_button.addEventListener("click",function () {
        poke(data.name);
    });
}
function pic2details(detail_col,pokemon){
    let getPoke = new XMLHttpRequest();
    getPoke.onreadystatechange = function(){
        if (done(this)){
            let data = JSON.parse(this.responseText);
            let picDiv = full_make("span",["pic_div"],detail_col);
            let newPic = fmake("img",picDiv);
            newPic.setAttribute("id",pokemon);
            picDiv.addEventListener("click",function () {
                poke(pokemon);
            });
            newPic.setAttribute("src",imageSauce(data.image));
            newPic.setAttribute("width",img_width);
            let name = fmake("h5",picDiv);
            name.innerText = data.name;
        }
    };
    getPoke.open("GET",iChooseYou(pokemon),true);
    getPoke.send();
}
function pokeDetails(){
    let pokeList = new XMLHttpRequest();
    let details = document.getElementById("details");
    wipe(details);
    let title = fmake("h4",details);
    title.innerText = "Pokemon details";
    pokeList.onreadystatechange = function(){
        if (done(this)){
            let plist = JSON.parse(this.responseText);
            for (let i = 0; i < plist.length; i++) {
                pic2details(details,plist[i]);
            }
        }
    };
    pokeList.open("GET",ENDPOINT_BASE_URL+"pokemon",true);
    pokeList.send();
}
function fillPoke(data,div){
    let name = fmake("h4",div);
    name.innerText = data.name;
    let pic_div = fmake("div",div);
    let pic = fmake("img",pic_div);
    pic.setAttribute("src",imageSauce(data.image));
    pic.setAttribute("width",(Number(img_width)*2)+"");
    let desc = fmake("p",div);
    desc.innerText = data.description;
}
function fillSide(data,div,type,heading){
    let head = fmake("h4",div);
    head.innerText = heading;
    let things = data.opponents[type];
    for (let i = 0; i < things.length; i++) {
        let get = new XMLHttpRequest();
        get.onreadystatechange = function(){
            if (done(this)){
                let pokedata = JSON.parse(this.responseText);
                let pic_div = full_make("div",["pic_div"],div);
                let pic = fmake("img",pic_div);
                pic.setAttribute("src",imageSauce(pokedata.image));
                pic.setAttribute("width",img_width);
                pic_div.style.margin = "2%";
                pic_div.addEventListener("click",function () {
                    poke(pokedata.name);
                });
                let name = fmake("h5",pic_div);
                name.innerText = pokedata.name;
            }
        };
        get.open("GET",iChooseYou(things[i]),true);
        get.send();
    }
}
function fillWeak(data,div){
    fillSide(data,div,"weak_against","Weak Against");
}
function fillStrong(data,div){
    fillSide(data,div,"strong_against","Strong Against");
}
function featurePoke(data,details_div){
    let weak = full_make("div",["weakness"],details_div);
    let pokemon = full_make("div",["featured"],details_div);
    let strong = full_make("div",["strength"],details_div);
    fillWeak(data,weak);
    fillPoke(data,pokemon);
    fillStrong(data,strong);
    loadStats(data,pokemon);
}
function poke(id){
    let div = document.getElementById("details");
    wipe(div);
    let pokedex = make("div");
    pokedex.classList.add("pokedex");
    let get = new XMLHttpRequest();
    get.onreadystatechange = function(){
        if (done(this)){
            let data = JSON.parse(this.responseText);
            let title = fmake("h4",div);
            title.innerText = "Pokemon details";
            featurePoke(data,pokedex);
            div.appendChild(pokedex)
        }
    };
    if (id) {
        get.open("GET", iChooseYou(id), true);
    } else{
        get.open("GET",ENDPOINT_BASE_URL+"pokemon?random=random",true);
    }
    get.send();
}
function setColour(element,keyword){
    let get = new XMLHttpRequest();
    let box = make("span");
    box.innerText = element.innerText;
    wipe(element);
    element.appendChild(box);
    get.onreadystatechange = function () {
        if (done(this)){
            let data = JSON.parse(this.responseText);
            box.style.color = data["foreground"];
            box.style.backgroundColor = data["background"];
            box.classList.add("pokemon_class");
        }
    };
    get.open("GET","https://sporadic.nz/pokesignment/keyword?keyword=" + keyword,true);
    get.send();
}
function addNewRows(data,table,isClass){
    let array;
    if (isClass){
        array = data["classes"];
    } else {
        array = data["signature_skills"];
    }
    for (let i = 0; i < array.length; i++) {
        let newSpan = full_make("div",["pokemon_details"],table);
        newSpan.innerText = array[i];
        if (isClass){
            setColour(newSpan,array[i]);
        }
    }
}
function fillTable(data,table,isClass){
    let header = fmake("h5",table);
    if (isClass) {
        header.innerText = "Class list";
    } else {
        header.innerText = "Signature Moves";
    }
    addNewRows(data,table,isClass);
}
function loadStats(data,div){
    let stat_div = full_make("div",["poke_stat_div"],div);
    let class_div = full_make("div",["poke_table","poke_class"],stat_div);
    let move_div = full_make("div",["poke_table","poke_move"],stat_div);
    fillTable(data,class_div,true);
    fillTable(data,move_div,false);
}
function main(){
    let burger_icon = document.getElementById("nav_menu_icon");
    let drop_nav_menu = document.getElementById("mobile_nav_menu");
    function close_nav_menu() {
        if (document.body.clientWidth > 900) {
            drop_nav_menu.style.display = "none";
        }
    }
    let close_mobile_nav = setInterval(close_nav_menu,100);
    function toggle_nav(){
        if (drop_nav_menu.style.display === "none"){
            drop_nav_menu.style.display = "block";
            close_mobile_nav = setInterval(close_nav_menu,100);
        } else {
            drop_nav_menu.style.display = "none";
            clearInterval(close_mobile_nav);
        }
    }
    burger_icon.addEventListener("click",toggle_nav);
    let homebutton = document.getElementById("nav_home");
    homebutton.addEventListener("click",function(){
        window.open("https://www.pokemon.com","_blank")});
    getRandomPoke(loadPokeToday);
    pokeDetails();
}